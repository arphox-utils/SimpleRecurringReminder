﻿using SimpleRecurringReminder.Misc;
using System;
using System.Windows;
using System.Windows.Input;

namespace SimpleRecurringReminder
{
    internal sealed class ViewModel : Bindable
    {
        public ICommand GoCommand { get; }
        public ICommand HelpCommand { get; }

        public Visibility WindowVisibility
        {
            get => _windowVisibility;
            set
            {
                _windowVisibility = value;
                OnPropertyChanged();
            }
        }

        public TimeSpan Interval { get; set; } = new TimeSpan(0, 10, 0);
        public string Message { get; set; }

        private RecurringReminder _recurringReminder;

        public ViewModel()
        {
            GoCommand = new RelayCommand(Go);
            HelpCommand = new RelayCommand(() => MessageBox.Show(Properties.Resources.help, "Help"));
        }

        private void Go()
        {
            WindowVisibility = Visibility.Collapsed;
            _recurringReminder = new RecurringReminder(Interval, Message, FinishCallback);
            _recurringReminder.Start();
        }

        private void FinishCallback()
        {
            _recurringReminder = null;
            WindowVisibility = Visibility.Visible;
        }

        #region [ Backing fields ]

        private Visibility _windowVisibility = Visibility.Visible;

        #endregion
    }
}